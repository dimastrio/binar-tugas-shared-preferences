package id.dimas.binartugassharedpreferences

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.dimas.binartugassharedpreferences.LoginFragment.Companion.KEY_LOGIN

class SplashFragment : Fragment() {

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        splashFunction()
    }

    private fun splashFunction() {
        sharedPref = requireContext().getSharedPreferences("ini_id", Context.MODE_PRIVATE)
        Handler(Looper.myLooper()!!).postDelayed({
            val login = sharedPref.getInt(KEY_LOGIN, 0)
            if (login == 0) {
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            } else {
                findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
            }
        }, 2000)
    }


}