package id.dimas.binartugassharedpreferences

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import id.dimas.binartugassharedpreferences.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("ini_id", Context.MODE_PRIVATE)

        loginFunction()
    }


    private fun loginFunction() {

        binding.btnLogin.setOnClickListener {
            val username = binding.editUsername.text.toString()
            val password = binding.editPassword.text.toString()
            val login = 1

            val editor = sharedPref.edit()

            editor.putInt(KEY_LOGIN, login)
            editor.putString(KEY_USERNAME, username)
            editor.putString(KEY_PASSWORD, password)

            editor.apply()

            if (username.isEmpty() && password.isEmpty()) {
                binding.editPassword.error = "Password Harus Diisi"
                binding.editUsername.error = "Username Harus Diisi"
            } else if (username.isEmpty()) {
                binding.editUsername.error = "Username Harus Diisi"
            } else if (password.isEmpty()) {
                binding.editPassword.error = "Password Harus Diisi"
            } else {
                it.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            }

            binding.editUsername.text.clear()
            binding.editPassword.text.clear()
        }
    }

    companion object {

        const val KEY_LOGIN = "login"
        const val KEY_USERNAME = "username"
        const val KEY_PASSWORD = "password"

    }
}