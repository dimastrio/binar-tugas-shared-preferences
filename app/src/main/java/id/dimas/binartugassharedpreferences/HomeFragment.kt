package id.dimas.binartugassharedpreferences

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import id.dimas.binartugassharedpreferences.LoginFragment.Companion.KEY_USERNAME
import id.dimas.binartugassharedpreferences.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("ini_id", Context.MODE_PRIVATE)

        showName()

        logoutFunction()
    }

    private fun showName() {

        val name = sharedPref.getString(KEY_USERNAME, "Default Username")

        binding.tvName.text = name
    }

    private fun logoutFunction() {
        binding.btnLogout.setOnClickListener {

            val editor = sharedPref.edit()

            editor.clear()

            editor.apply()

            it.findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
        }
    }


}